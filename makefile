#OBJS specifies which files to compile as part of the project
OBJS = test.cpp
#CC specifies which compiler we're using
CC = g++
#OPT specifies optimization flags
OPT = -g -std=c++17
#COMPILER_FLAGS specifies the additional compilation options we're using
# -w suppresses all warnings
COMPILER_FLAGS = -Wall
#LINKER_FLAGS specifies the libraries we're linking against
LINKER_FLAGS = #no links
#OBJ_NAME specifies the name of our exectuable
OBJ_NAME = test
#This is the target that compiles our executable
all : $(OBJS)
	$(CC) $(OPT) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)
