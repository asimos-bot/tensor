#include<iostream>

#define debug fprintf(stderr, "\n%s: line %d\n\n", __FILE__, __LINE__);
#include "jkr/jkr.hpp"

int main(){

	auto show = [](v<float, 1>& t){
		printf("[");
		for(float i : t) printf(" %f ", i);
		printf("]\n");
	};


	auto show2 = [show](v<float, 2>& t){

		printf("[");
		for(v<float, 1>& i : t) show(i);
		printf("]\n");
	};

	auto show3 = [show2](v<float, 3>& t){

		printf("[");
		for(v<float, 2>& i : t) show2(i);
		printf("]\n");
	};
	
	auto show4 = [show3](v<float, 4>& t){
		unsigned int i;
		printf("[");
		for(i=0; i<t.size(); i++) show3(t[i]);
		printf("]\n");
	};

	std::shared_ptr<Shape> v1_shape( new Shape(3) );
	(*v1_shape)[0] = 3;
	(*v1_shape)[1] = 4;
	(*v1_shape)[2] = 2;

	v<float, 3> v1( {}, v1_shape );
	v1[0][1][2] = 3;
	v1[1][3][2] = 4;

	printf("v1:\n");
	show3(v1);

	printf("v1 num_owners before copy: %d\n", v1.num_owners());
	
	v<float, 3> v2{};
	v2.copy( v1 );
	v2 = v2.T();
	printf("v1 num_owners after copy: %d\n", v1.num_owners());
	printf("v2:\n");
	show3(v2);
	printf("v2 num_owners: %d\n", v2.num_owners());

	std::shared_ptr<Shape> v3_shape( new Shape(2) );
	(*v3_shape)[0]=4;
	(*v3_shape)[1]=3;
	v<float, 2> v3( {}, v3_shape );
	v3[0][2] = 16;
	show2(v3);
	
	v2.append_row( v3 );
	printf("v2.append_row(v3):\n");
	v2[2][1][0] = 189;
	v2[2][2][3] = 29009;
	show3(v2);
	printf("v2 num owners after v2.append_row(v3): %d\n", v2.num_owners());

	v2.append_column(v2[2]);
	printf("v2.append_columns(v2[2]):\n");
	show3(v2);
	printf("v2 num owners after v2.append_column(v2[2]): %d\n", v2.num_owners());

	v<float, 2> v4 = v2[2].cut(1, 3, 0 , 2);
	printf("v4:\n");
	show2(v4);
	printf("v2 after add 1 to everything:\n");
	v2 = v2.apply( [](float a, int b){ return a+1; } );
	show3(v2);
	v<float, 3> v5{};
	v5.copy(v2);
	printf("v2 num owners after v2.apply( [](float a, int b){ return a+1; } ): %d\n", v2.num_owners());
	
	printf("v2 after add 2 to everything:\n");

	show3(v2);
	printf("v2 num owners after v2 + 2: %d\n", v2.num_owners());
	printf("v5:\n");
	show3(v5);
	printf("v5 owners:%d\n", v5.num_owners());
	printf("v2 with operation with v5:\n");
	v2 /= v5;
	show3(v2);
	printf("v2 owners: %d\n", v2.num_owners());
	printf("v5 owners: %d\n", v5.num_owners());

	v<float, 1>v6 = v4[0].copy();

	v<float, 1>v7({ 3 });
	v7[0]=2;
	v7[1]=3;
	v7[2]=1;
	printf("%f\n", dot(v7, v7));

auto int_show = [](v<int, 1>& t){
		unsigned int i;
		printf("[");
		for(i=0; i<t.size(); i++) printf(" %d ", t[i]);
		printf("]\n");
	};


	auto int_show2 = [int_show](v<int, 2>& t){
		unsigned int i;
		printf("[");
		for(i=0; i<t.rows(); i++) int_show(t[i]);
		printf("]\n");
	};

	v<float, 2> v8({ 2,3 });
	v8.rand(0,1);

	v<float, 2> v9({ 4, 2 });
	v9.rand(0,1);

	v<float, 2> v10({ 2,3 });

	v10.load("idk");

	show2(v10);

	for( auto i : v10[0] ) printf("%f\n", i);
	return 0;

}
