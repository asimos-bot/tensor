#ifndef ALERT_H
#define ALERT_H

void printerr(char* msg){

	char* str=(char*)malloc(17+strlen(msg));
	sprintf(str, "%c[%dm%c[%dm%s\n%c[%dm", 0x1B, 31, 0x1B, 1, msg, 0x1B, 0);

	fprintf(stderr, str, strlen(str));
	free(str);
}
void printsucess(char* msg){

	char* str=(char*)malloc(17+strlen(msg));
	sprintf(str, "%c[%dm%c[%dm%s\n%c[%dm", 0x1B, 32, 0x1B, 1, msg, 0x1B, 0);

	fprintf(stderr, str, strlen(str));
	free(str);
}
void printalert(char* msg){

	char* str=(char*)malloc(17+strlen(msg));
	sprintf(str, "%c[%dm%c[%dm%s\n%c[%dm", 0x1B, 33, 0x1B, 1, msg, 0x1B, 0);

	fprintf(stderr, str, strlen(str));
	free(str);
}

#endif
