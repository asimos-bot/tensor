#ifndef SHAPE_HPP
#define SHAPE_HPP

#include<signal.h> //raise(SIGSEGV)

namespace jkr{
	class Shape{
		private:
			//smart pointer, avoid leaking memory
			std::unique_ptr<unsigned int[]> vec;
			unsigned int _size;
		public:
			//constructor
			Shape(int initial_size):_size(initial_size){
				this->vec = std::make_unique<unsigned int[]>( _size );
			}
			//return size
			unsigned int size(){
				return this->_size;
			}
			//return element at given index
			unsigned int& operator[](unsigned int index){
				if( index < 0 || index >= this->_size ) raise(SIGSEGV);
				return this->vec.get()[index];
			}
			//copy from another Shape instance
			void copy(Shape& other){
				unsigned int i;
				for(i=0; i<other.size(); i++) this->vec.get()[i] = other[i];
			}
			//copy from memory
			void copy_from(int* ptr, int size){
			
				int i;

				for(i=0; i<size; i++) this->vec.get()[i] = ptr[i];
			}
			//copy to memory
			void copy_to(int* ptr, int size){
			
				int i;
				for(i=0; i<size; i++) ptr[i] = this->vec.get()[i];
			}
	};
}

#endif
