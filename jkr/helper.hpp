#ifndef HELPER_HPP
#define HELPER_HPP

namespace jkr{

	//create dir if it doesn't exist
	void handle_dir(char* dir_name){

		struct stat st = {0};

		if (stat(dir_name, &st) == -1) {
		    mkdir(dir_name, 0700);
		}
	}

	std::string add_prefix(const char* prefix, const char* str){

		std::string filepath(prefix);

		filepath += str;

		return filepath;
	}
}

#endif
