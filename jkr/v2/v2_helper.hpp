#ifndef V2_HELPER_HPP
#define V2_HELPER_HPP

#include<signal.h>//for raise(SIGSEGV)
#include<cmath> //for exp()

namespace jkr{

	//return matrix multiplication result from the two matrices 
	template<typename dtype>
	v<dtype, 2> dot( v<dtype, 2>&, v<dtype, 2>&);

	template<typename dtype>
	v<dtype, 2> dot( v<dtype, 2>& m1, v<dtype, 2>& m2 ){

		if( m1.columns() != m2.rows() ) raise(SIGSEGV);

		v<dtype, 2> result({ m2.columns(), m1.rows() });
		
		unsigned int i, j;

		for(i=0; i<m1.rows(); i++) for(j=0; j<m2.columns(); j++) result[i][j] = dot( m1[i], m2.cut(0, m2.rows(), j, j+1 ).T()[0] );

		return result;
	}
}

#endif
