#ifndef V2_HPP
#define V2_HPP

namespace jkr{

	template<typename dtype>
	class v<dtype, 2>{

		private:
			std::unique_ptr<v<dtype, 1>[]> vector;
			std::shared_ptr<Shape> shape;

		public:
			v(std::initializer_list<unsigned int> list={}, std::shared_ptr<Shape> shape_arr = nullptr):shape(shape_arr){

				if( list.size()!=0 ){

					this->shape.reset( new Shape(2) );
					(*this->shape)[0] = list.begin()[0];
					(*this->shape)[1] = list.begin()[1];
				}
				
				if(this->shape!=nullptr){
					//allocate memory for the vector
					this->vector = std::make_unique<v<dtype, 1>[]>( (*this->shape)[1] );

					unsigned int i;
					//iterate through pointers pointed by this->vector and allocate space for and create a
					//v<dtype, 1> obj for each one

					for(i=0; i<(*this->shape)[1]; i++) this->vector[i] = v<dtype, 1>( {}, this->shape );

				}
			}
			/*
				implementing range-based for
			*/
			//returns pointer to first value
			v<dtype, 1>* begin(){
				return (*this->shape)[1] > 0 ? &this->vector[0] : nullptr;
			}
			//returns pointer imediatly after the last value
			v<dtype, 1>* end(){
				return (*this->shape)[1] > 0 ? &this->vector[(*this->shape)[1]] : nullptr;
			}
			/*
				implementing range-based for
			*/

			//return number of owners of shape space memory
			int num_owners();

			//return number of rows
			unsigned int rows();

			//also return number of rows
			unsigned int size();

			//return number of columns
			unsigned int columns();

			//return value at index
			v<dtype, 1>& operator[](unsigned int);
			
			//copy matrix without taking ownership
			void copy( v<dtype, 2>&, bool=false);

			//returns copy of the matrix
			v<dtype, 2> copy();

			//return sum of all elements in matrix
			dtype sum();
			
			//return transposed matrix
			v<dtype, 2> T();
			
			//append vector to end of matrix (at this->vector[ this->n_rows ])
			void append_row( v<dtype, 1>& );
			
			//add new column (new element to the end of each vector)
			void append_column( v<dtype, 1>& );
			
			//fill the matrix with random numbers between the given range
			void rand(dtype, dtype);

			//return highest value in tensor
			dtype amax();

			//return lowest value in tensor
			dtype amin();

			//turn matrix into a tensor
			v<dtype, 3> add_dimension();
			
			//return copy of part of the matrix
			v<dtype, 2> cut(int, int, int, int);
			
			//apply given func to all elements of the matrix and return result
			v<dtype, 2> apply( dtype (*)( dtype, int ) );
			
			//apply a given function element-wise on a current matrix and a given matrix
			v<dtype, 2> apply( v<dtype, 2>& , dtype(*)( dtype, dtype ) );
			
			//save matrix in binary file
			void save(const char* filename, bool=true);
			
			//load matrix from binary file
			void load(const char* filename, bool=true);
			
			//change sign
			v<dtype, 2> operator-();
			
			/*
				scalar operations
			*/
			//addition
			v<dtype, 2> operator+(dtype);
			void operator+=(dtype);

			//subtraction
			v<dtype, 2> operator-(dtype);
			void operator-=(dtype);

			//multiplication
			v<dtype, 2> operator*(dtype);
			void operator*=(dtype);

			//division
			v<dtype, 2> operator/(dtype);
			void operator/=(dtype);

			/*
				matrix operations
			*/
			//addition
			v<dtype, 2> operator+( v<dtype, 2>& );
			void operator+=( v<dtype, 2>& );

			//subtraction
			v<dtype, 2> operator-( v<dtype, 2>& );
			void operator-=( v<dtype, 2>& );

			//multiplication
			v<dtype, 2> operator*( v<dtype, 2>& );
			void operator*=( v<dtype, 2>& );

			//division
			v<dtype, 2> operator/( v<dtype, 2>& );
			void operator/=( v<dtype, 2>& );

			/*
				scalar comparative operators
			*/
			v<dtype, 2> operator<(dtype);
			v<dtype, 2> operator>(dtype);
	};
}

#include "v2_methods.hpp"
#include "v2_helper.hpp"

#endif
