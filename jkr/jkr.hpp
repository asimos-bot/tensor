#ifndef JKR_HPP
#define JKR_HPP

#include<stdio.h> //file i/o
#include<stdlib.h> //dynamic memory
#include<string.h> //string handling for c
#include<string> //string handling for c++
#include<memory> // smart pointers
#include<random> //generate random numbers
#include<limits.h> //get limits like max filepath length

#include <sys/types.h> //
#include <sys/stat.h> // for handle_dir function in helper.hpp
#include <unistd.h> //

#include "alert.hpp" //display colored messaged in case of errors

#include "helper.hpp" //helper functions for file I/O operations

#include "v/v.hpp"
#include "v1/v1.hpp"
#include "v2/v2.hpp"

#endif
