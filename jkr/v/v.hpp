#ifndef V_HPP
#define V_HPP

#include "../shape.hpp"

namespace jkr{

	template<typename dtype, int axis=0>
	class v{
	
		private:

			std::shared_ptr<Shape> shape;
			std::unique_ptr<v<dtype, axis-1>[]> vector;

		public:
			//constructor
			v(std::initializer_list<unsigned int> list={}, std::shared_ptr<Shape> shape_arr = nullptr):shape(shape_arr){

				unsigned int i;

				if( list.size() != 0 ){
					this->shape.reset( new Shape(axis) );

					for(i=0; i<axis; i++) (*this->shape)[i] = list.begin()[i];
				}
				if(this->shape!=nullptr){

					//allocate memory for the vector
					this->vector = std::make_unique<v<dtype, axis-1>[]>( (*this->shape)[axis-1] );

					//iterate through pointers pointed by this->vector and allocate space for and create a
					//v<dtype, axis-1> obj for each one

					for(i=0; i<(*this->shape)[axis-1]; i++) this->vector[i] = v<dtype, axis-1>( {}, this->shape );
				}
			}
			/*
				implementing range-based for
			*/
			//returns pointer to first value
			v<dtype, axis-1>* begin(){
				return (*this->shape)[axis-1] > 0 ? &this->vector[0] : nullptr;
			}
			//returns pointer imediatly after the last value
			v<dtype, axis-1>* end(){
				return (*this->shape)[axis-1] > 0 ? &this->vector[(*this->shape)[axis-1]] : nullptr;
			}
			/*
				implementing range-based for
			*/
			//return number of owners of shape space memory
			int num_owners();
			
			//number of vectors
			unsigned int size();

			//return value at index
			v<dtype, axis-1>& operator[](unsigned int);

			//copy matrix without taking ownership
			void copy( v<dtype, axis>&, bool=false);

			//returns a copy of the tensor
			v<dtype, axis> copy();

			//return sum of all elements in tensor
			dtype sum();
			
			//return transposed matrix
			v<dtype, axis> T();
			
			//append vector to end of matrix (at this->vector[ (*this->shape)[axis] ])
			void append_row( v<dtype, axis-1>& );
			
			//add new column (new v<dtype, axis-1>( this->shape ) to the end of each vector)
			void append_column( v<dtype, axis-1>& );
			
			//fill the tensor with random numbers in the given range
			void rand(dtype begin, dtype end);
			
			//return highest value in tensor
			dtype amax();

			//return lowest value in tensor
			dtype amin();

			//turn this axis-d tensor into a (axis+1)-d tensor
			v<dtype, axis+1> add_dimension();
			
			//apply given func to all elements of the tensor and return result
			v<dtype, axis> apply( dtype (*)( dtype, int ) );
			
			//apply a given function element-wise on a current tensor and a given tensor
			v<dtype, axis> apply( v<dtype, axis>& , dtype(*)( dtype, dtype ) );
			
			//save tensor in binary file
			void save(const char* filename, bool=true);
			
			//load tensor from binary file
			void load(const char* filename, bool=true);

			//change sign
			v<dtype, axis> operator-();

			/*
				scalar operations
			*/
			//addition
			v<dtype, axis> operator+(dtype);
			void operator+=(dtype);

			//subtraction
			v<dtype, axis> operator-(dtype);
			void operator-=(dtype);

			//multiplication
			v<dtype, axis> operator*(dtype);
			void operator*=(dtype);

			//division
			v<dtype, axis> operator/(dtype);
			void operator/=(dtype);

			/*
				matrix operations
			*/
			//addition
			v<dtype, axis> operator+( v<dtype, axis>& );
			void operator+=( v<dtype, axis>& );

			//subtraction
			v<dtype, axis> operator-( v<dtype, axis>& );
			void operator-=( v<dtype, axis>& );

			//multiplication
			v<dtype, axis> operator*( v<dtype, axis>& );
			void operator*=( v<dtype, axis>& );

			//division
			v<dtype, axis> operator/( v<dtype, axis>& );
			void operator/=( v<dtype, axis>& );
			
			/*
				scalar comparative operators
			*/
			v<dtype, axis> operator<(dtype);
			v<dtype, axis> operator>(dtype);
	};

}

#include "v_methods.hpp"

#endif
