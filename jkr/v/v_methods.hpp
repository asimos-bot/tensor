#ifndef V_METHODS_HPP
#define V_METHODS_HPP

#include<signal.h>//raise(SIGSEGV)

using namespace jkr;

template<typename dtype, int axis>
int v<dtype, axis>::num_owners(){
	return this->shape.use_count();
}

template<typename dtype, int axis>
unsigned int v<dtype, axis>::size(){
	return (*this->shape)[axis-1];
}

template<typename dtype, int axis>
v<dtype, axis-1>& v<dtype, axis>::operator[](unsigned int index){

	if(index >= (*this->shape)[axis-1]){
		printerr((char*)"Out of bounds index requested for v");
		raise(SIGSEGV);
	}

	return this->vector[index];
}

template<typename dtype, int axis>
void v<dtype, axis>::copy( v<dtype, axis>& to_clone, bool realloc ){

	unsigned int i;
	if( this->shape == nullptr ) this->shape.reset( new Shape(axis) );

	//if sizes are not equal, make them be so
	if( realloc || (*this->shape)[axis-1] != (*to_clone.shape)[axis-1] ){

		//modify shape
		(*this->shape)[axis-1] = (*to_clone.shape)[axis-1];
		/*rearrange memory*/
		//allocate memory for the vector
		this->vector = std::make_unique<v<dtype, axis-1>[]>( (*this->shape)[axis-1] );

		//iterate through pointers pointed by this->vector and allocate space for and create a
		//v<dtype, axis-1> obj for each one
		for(i=0; i<(*this->shape)[axis-1]; i++) this->vector[i] = v<dtype, axis-1>( {}, this->shape );
	}

	for(i=0; i<(*this->shape)[axis-1]; i++) this->vector[i].copy( to_clone[i], true );
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::copy(){

	v<dtype, axis> clone;
	clone.copy( *this );
	return clone;
}

template<typename dtype, int axis>
dtype v<dtype, axis>::sum(){

	dtype sum=0;
	int i;
	for(i=0; i<(*this->shape)[axis-1]; i++) sum+=this->vector[i].sum();
	return sum;
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::T(){

	std::shared_ptr<Shape> result_shape( new Shape( axis ) );
	(*result_shape)[axis-1] = (*this->shape)[axis-1];
	(*result_shape)[axis-2] = (*this->shape)[axis-2];
	v<dtype, axis> result( {}, result_shape );

	unsigned int i;
	for(i=0; i<(*result_shape)[axis-1]; i++){
		v<dtype, axis-1> tmp = this->vector[i].T();
		result[i].copy( tmp );
	}
	
	return result;
}

template<typename dtype, int axis>
void v<dtype, axis>::append_row(v<dtype, axis-1>& vec){
	int i;
	if( (*this->shape)[axis-2] != vec.size() ){
		printerr((char*)"Incompatible size of given vec in v->append_row()");
		raise(SIGSEGV);
	}
	//current size
	int initial_size = (*this->shape)[axis-1];

	//update num of rows
	(*this->shape)[axis-1]++;

	//create a v<dtype, axis>
	v<dtype, axis> new_vec( {}, this->shape );

	//iterate through new_vec and copy vectors from current this->vector
	for(i=0; i<initial_size; i++) new_vec[i].copy( this->vector[i] );

	//add new vec to the end of new_vec
	new_vec[initial_size].copy( vec );

	//copy new_vec
	this->copy( new_vec, true );
}


template<typename dtype, int axis>
void v<dtype, axis>::append_column( v<dtype, axis-1>& vec ){

	if( (*this->shape)[axis-1] != vec.size() ){
		printerr((char*)"Incompatible size of given vec in v->append_column()");
		raise(SIGSEGV);
	}

	//iterate through rows and append element to the end of them
	unsigned int i;
	for(i=0; i<(*this->shape)[axis-1]; i++){

		this->vector[i].append_row( vec[i] );

		/*the append method for v1 add 1 to the column size in shape array
		since the address of this value is shared among the v1 in this matrix
		each one will add 1 in each iteration of this loop!
		so we counter this by decreasing it by 1 here and then adding one
		after the loop*/
		(*this->shape)[axis-2]--;
	}
	(*this->shape)[axis-2]++;
}

template<typename dtype, int axis>
void v<dtype, axis>::rand(dtype begin, dtype end){

	int i;
	for(i=0; i<(*this->shape)[axis-1]; i++) this->vector[i].rand(begin, end);
}

template<typename dtype, int axis>
dtype v<dtype, axis>::amax(){

	dtype highest=this->vector[0].amax();
	dtype current;
	
	int i;
	for(i=1; i<(*this->shape)[axis-1]; i++){
		current = this->vector[i].amax();
		if( current > highest ) highest = current;
	}
	return highest;
}

template<typename dtype, int axis>
dtype v<dtype, axis>::amin(){

	dtype lowest=this->vector[0].amin();
	dtype current;
	
	int i;
	for(i=1; i<(*this->shape)[axis-1]; i++){
		current = this->vector[i].amin();
		if( current > lowest ) lowest = current;
	}
	return lowest;
}

template<typename dtype, int axis>
v<dtype, axis+1> v<dtype, axis>::add_dimension(){

	std::shared_ptr<Shape> result_shape( new Shape(axis+1) );

	(*result_shape).copy( (*this->shape) );
	(*result_shape)[axis]=1;

	v<dtype, axis+1> result( {}, result_shape );
	result[0].copy( *this );
	
	return result;
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::apply( dtype (*func)( dtype, int ) ){

	unsigned int i;

	//creating new vector in which we'll store the results
	std::shared_ptr<Shape> result_shape( new Shape(axis) );
	(*result_shape).copy( (*this->shape) );
	v<dtype, axis> result( {}, result_shape );
	v<dtype, axis-1> tmp{};
	
	for(i=0; i<(*this->shape)[axis-1]; i++){
		tmp = this->vector[i].apply( func );
		result[i].copy( tmp );
	}
	
	return result;
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::apply( v<dtype, axis>& other, dtype(*func)( dtype, dtype ) ){

	if( (*this->shape)[axis-1] != other.size() ){
		printerr((char*)"Incompatible size of given tensor in v->apply()");
		raise(SIGSEGV);	
	}

	unsigned int i;

	//creating new matrix in which we'll store the results
	std::shared_ptr<Shape> result_shape( new Shape(axis) );
	(*result_shape).copy( (*this->shape) );

	v<dtype, axis> result( {}, result_shape );
	
	//iterate through the rows and apply the 'apply' function (v1) to each row
	//saving the result in a vector from the matrix
	v<dtype, axis-1> tmp{};
	for(i=0; i<(*this->shape)[axis-1]; i++){
		tmp = this->vector[i].apply(other[i], func);
		result[i].copy( tmp );
	}
	return result;
}

template<typename dtype, int axis>
void v<dtype, axis>::save(const char* raw_filename, bool save_shape){

	char current_filename[PATH_MAX];

	if(save_shape){

		handle_dir((char*)"jkr_data/");

		//get shape file name
		std::string shape_filename = "jkr_data/" ;
		shape_filename += raw_filename;
		shape_filename += ".jkr.shape";

		//get shape file
		FILE* shape_file = fopen(shape_filename.c_str(), "wb");

		unsigned int shape_copy[2] = { (*this->shape)[0], (*this->shape)[1] };

		//write shape to file
		fwrite( &shape_copy, sizeof(unsigned int), 2, shape_file );

		fclose(shape_file);

	}

	int i;
	const char* filename;
	for(i=0; i<(*this->shape)[1]; i++){

		sprintf(current_filename, "%s_%d", raw_filename, i);
		filename = current_filename;
		this->vector[i].save( filename, false);
	}
}

template<typename dtype, int axis>
void v<dtype, axis>::load(const char* raw_filename, bool load_shape){

	char current_filename[PATH_MAX];

	if(load_shape){

		//get shape file name
		std::string shape_filename("jkr_data/");
		shape_filename += raw_filename;
		shape_filename += ".jkr.shape";

		//get shape file
		FILE* shape_file = fopen(shape_filename.c_str(), "rb");

		//get shape from file
		unsigned int shape_copy[2];

		fread(&shape_copy, sizeof(unsigned int), 2, shape_file);

		(*this->shape)[0] = shape_copy[0];
		(*this->shape)[1] = shape_copy[1];
		
		fclose(shape_file);
	}

	int i;
	const char* filename;
	for(i=0; i<(*this->shape)[1]; i++){

		sprintf(&current_filename[0], "%s_%d", raw_filename, i);
		filename = current_filename;
		this->vector[i].load( filename, false);
	}
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator-(){

	return this->apply([](dtype element, int){
		return -element;
	});
}

/*


	scalar operations


*/

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator+( dtype scalar ){

	int i;

	//creating matrix in which we'll store the results
	std::shared_ptr<Shape> result_shape( new Shape(axis) );
	(*result_shape).copy( (*this->shape) );
	v<dtype, axis> result( {}, result_shape );

	//iterate through matrix and add to rows
	v<dtype, axis-1> tmp;
	for(i=0; i<(*this->shape)[axis-1]; i++){
		tmp = this->vector[i] + scalar;
		result[i].copy( tmp );
	}
	return result;
}

template<typename dtype, int axis>
void v<dtype, axis>::operator+=( dtype scalar){

	v<dtype, axis> result = *this + scalar;
	this->copy( result );
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator-( dtype scalar ){

	int i;

	//creating matrix in which we'll store the results
	std::shared_ptr<Shape> result_shape( new Shape(axis) );
	(*result_shape).copy( (*this->shape) );
	v<dtype, axis> result( {}, result_shape );

	//iterate through matrix and add to rows
	v<dtype, axis-1> tmp;
	for(i=0; i<(*this->shape)[axis-1]; i++){
		tmp = this->vector[i] - scalar;
		result[i].copy( tmp );
	}
	return result;
}

template<typename dtype, int axis>
void v<dtype, axis>::operator-=( dtype scalar){

	v<dtype, axis> result = *this - scalar;
	this->copy( result );
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator*( dtype scalar ){

	int i;

	//creating matrix in which we'll store the results
	std::shared_ptr<Shape> result_shape( new Shape(axis) );
	(*result_shape).copy( (*this->shape) );
	v<dtype, axis> result( {}, result_shape );

	//iterate through matrix and add to rows
	v<dtype, axis-1> tmp;
	for(i=0; i<(*this->shape)[axis-1]; i++){
		tmp = this->vector[i] * scalar;
		result[i].copy( tmp );
	}
	return result;
}

template<typename dtype, int axis>
void v<dtype, axis>::operator*=( dtype scalar){

	v<dtype, axis> result = *this * scalar;
	this->copy( result );
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator/( dtype scalar ){

	int i;

	//creating matrix in which we'll store the results
	std::shared_ptr<Shape> result_shape( new Shape(axis) );
	(*result_shape).copy( (*this->shape) );
	v<dtype, axis> result( {}, result_shape );

	//iterate through matrix and add to rows
	v<dtype, axis-1> tmp;
	for(i=0; i<(*this->shape)[axis-1]; i++){
		tmp = this->vector[i] / scalar;
		result[i].copy( tmp );
	}
	return result;
}

template<typename dtype, int axis>
void v<dtype, axis>::operator/=( dtype scalar){

	v<dtype, axis> result = *this / scalar;
	this->copy( result );
}

/*


	matrix operations


*/

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator+( v<dtype, axis>& other ){

	return this->apply( other, []( dtype a, dtype b) -> dtype{
		return a+b;
	} );
}

template<typename dtype, int axis>
void v<dtype, axis>::operator+=( v<dtype, axis>& other ){

	v<dtype, axis> result = *this + other;
	this->copy( result );
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator-( v<dtype, axis>& other ){
	return this->apply( other, []( dtype a, dtype b) -> dtype{
		return a-b;
	} );
}

template<typename dtype, int axis>
void v<dtype, axis>::operator-=( v<dtype, axis>& other ){

	v<dtype, axis> result = *this - other;
	this->copy( result );
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator*( v<dtype, axis>& other ){
	return this->apply( other, []( dtype a, dtype b) -> dtype{
		return a*b;
	} );
}

template<typename dtype, int axis>
void v<dtype, axis>::operator*=( v<dtype, axis>& other ){

	v<dtype, axis> result = *this * other;
	this->copy( result );
}

template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator/( v<dtype, axis>& other ){

	return this->apply( other, []( dtype a, dtype b) -> dtype{
		return a/b;
	} );
}

template<typename dtype, int axis>
void v<dtype, axis>::operator/=( v<dtype, axis>& other ){

	v<dtype, axis> result = *this / other;
	this->copy( result );
}

/*
	scalar comparative operators
*/
template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator<(dtype num){

	std::shared_ptr<Shape> result_shape( new Shape(axis) );
	(*result_shape).copy(this->shape);
	v<dtype, axis> result({}, result_shape);
	unsigned int i;
	for(i=0; i<(*this->shape)[axis-1]; i++) result[i] = this->vector[i] < num;

	return result;
}
template<typename dtype, int axis>
v<dtype, axis> v<dtype, axis>::operator>(dtype num){

	std::shared_ptr<Shape> result_shape( new Shape(axis) );
	(*result_shape).copy(this->shape);
	v<dtype, axis> result({}, result_shape);
	unsigned int i;
	for(i=0; i<(*this->shape)[axis-1]; i++) result[i] = this->vector[i] > num;

	return result;
}

#endif
