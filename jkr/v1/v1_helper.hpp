#ifndef V1_HELPER_HPP
#define V1_HELPER_HPP

namespace jkr{

	//return dot product between the two vectors 
	template<typename dtype>
	dtype dot( v<dtype, 1>&, v<dtype, 1>&);

	template<typename dtype>
	dtype dot( v<dtype, 1>& vec1, v<dtype, 1>& vec2 ){

		v<dtype, 1> result = vec1 * vec2;

		return result.sum();
	}
}

#endif
