#ifndef V1_METHODS_HPP
#define V1_METHODS_HPP

#include<signal.h>//for raise(SIGSEGV)

using namespace jkr;

template<typename dtype>
int v<dtype, 1>::num_owners(){
	return this->shape.use_count();
}

template<typename dtype>
unsigned int v<dtype, 1>::size(){
	return (*this->shape)[0];
}

template<typename dtype>
dtype& v<dtype, 1>::operator[](unsigned int idx){

	//raise segmantation fault if the index is out of bounds
	if( idx >= (*this->shape)[0] || !this->vector ){
		printerr((char*)"Out of bounds index requested for v1");
		raise(SIGSEGV);
	}

	return this->vector[idx];
}

template<typename dtype>
void v<dtype, 1>::copy(v<dtype, 1>& to_clone, bool realloc ){

	unsigned int i;
	//if there's no Shape, create one
	if( this->shape == nullptr ) this->shape.reset( new Shape(1) );

	//if vectors sizes are different
	if( realloc || (*this->shape)[0] != to_clone.size()){

		//update size
		(*this->shape)[0] = to_clone.size();

		//allocate new memory space
		this->vector = std::make_unique<dtype[]>( (*this->shape)[0] );
	}

	//iterating through the vector and copying the values
	for(i=0; i<(*this->shape)[0]; i++) this->vector[i] = to_clone[i];
}

template<typename dtype>
v<dtype, 1> v<dtype, 1>::copy(){

	v<dtype, 1> clone;
	clone.copy( *this );
	return clone;
}

template<typename dtype>
dtype v<dtype, 1>::sum(){

	dtype sum=0;
	unsigned int i;
	for(i=0; i<(*this->shape)[0]; i++) sum+=this->vector[i];
	return sum;
}

template<typename dtype>
void v<dtype, 1>::append(dtype new_value){

	int i;

	//tmp shape used for the transition from old vector to new one
	dtype* new_vector = new dtype[(*this->shape)[0]+1];

	//copy this->vector to new_vector
	for(i=0; i<(*this->shape)[0]; i++) new_vector[i] = this->vector[i];

	//add new_value to the end of new_vector
	new_vector[i] = new_value;

	//update size
	(*this->shape)[0]++;

	//make this->vector point to new_vector and deallocate previous memory space
	this->vector.reset( new_vector );
}

template<typename dtype>
void v<dtype, 1>::rand(dtype begin, dtype end){

	std::uniform_real_distribution<dtype> dist( begin, end );

	unsigned int i;
	for(i=0; i<(*this->shape)[0]; i++) this->vector[i] = dist(this->mt);
}

//g++ bug, specialization in this case can only be done inside namespace block :P
namespace jkr{
template<>
void v<int, 1>::rand(int begin, int end){

	std::uniform_int_distribution<int> dist( begin, end );

	unsigned int i;
	for(i=0; i<(*this->shape)[0]; i++) this->vector[i] = dist(this->mt);
}
}

template<typename dtype>
dtype v<dtype, 1>::amax(){

	dtype highest=this->vector[0];
	dtype current;
	
	int i;
	for(i=1; i<(*this->shape)[0]; i++){
		current = this->vector[i];
		if( current > highest ) highest = current;
	}
	return highest;
}

template<typename dtype>
dtype v<dtype, 1>::amin(){

	dtype lowest=this->vector[0];
	dtype current;
	
	int i;
	for(i=1; i<(*this->shape)[0]; i++){
		current = this->vector[i];
		if( current > lowest ) lowest = current;
	}
	return lowest;
}

template<typename dtype>
v<dtype, 2> v<dtype, 1>::add_dimension(){

	std::shared_ptr<Shape> result_shape( new Shape(2) );
	(*result_shape)[0] = (*this->shape)[0];
	(*result_shape)[1] = 1;
	v<dtype, 2> result( {}, result_shape );
	result[0].copy( *this );
	
	return result;
}

template<typename dtype>
v<dtype, 1> v<dtype, 1>::cut(int begin, int end){

	std::shared_ptr<Shape> result_shape( new Shape(1) );
	(*result_shape)[0]=end-begin;
	if( (*result_shape)[0]<0 ){
		printerr((char*)"Invalid vector size for result vector in v1->cut()");
		raise(SIGSEGV);
	}
	v<dtype, 1> result( {}, result_shape );
	
	int i, counter;
	for(i=begin, counter=0; i<end; i++,counter++) result[counter] = this->vector[i];
	
	return result;
}

template<typename dtype>
v<dtype, 1> v<dtype, 1>::apply( dtype (*func)( dtype, int ) ){

	unsigned int i;
	
	//creating new vector in which we'll store the results
	std::shared_ptr<Shape> new_vec_shape( new Shape(1) );
	(*new_vec_shape)[0] = (*this->shape)[0];
	v<dtype, 1> result( {}, new_vec_shape );

	//iterate through the vector and apply the function to each value
	//saving the result in a new vector
	for(i=0; i<(*this->shape)[0]; i++) result[i] = func( this->vector[i], i );

	return result;
}

template<typename dtype>
v<dtype, 1> v<dtype, 1>::apply( v<dtype, 1>& other, dtype (*func)( dtype, dtype ) ){

	if( other.size() != (*this->shape)[0] ){
		printerr((char*)"Incompatible size of given vector in v1->apply()");
		raise(SIGSEGV);
	}

	//create result vector with appropriate size
	std::shared_ptr<Shape> result_shape( new Shape( 1 ) );
	(*result_shape)[0] = other.size();
	v<dtype, 1> result( {}, result_shape );

	unsigned int i;

	//iterate through both vectors and apply the function to each of their elements
	//storing the result in the result vector
	for(i=0; i<(*this->shape)[0]; i++) result[i] = func( this->vector[i], other[i] );

	return result;
}

template<typename dtype>
void v<dtype, 1>::save(const char* raw_filename, bool save_shape){

	std::string filename = add_prefix("jkr_data/", raw_filename);

	if(save_shape){

		handle_dir((char*)"jkr_data/");

		//get shape file name
		std::string shape_filename = filename + ".shape";

		//get shape file
		FILE* shape_file = fopen(shape_filename.c_str(), "wb");

		unsigned int shape_copy = (*this->shape)[0];

		//write shape to file
		fwrite( &shape_copy, sizeof(unsigned int), 1, shape_file );
		fclose(shape_file);
	}

	filename += ".jkr";

	FILE* file = fopen(filename.c_str(), "wb");

	//write vector to file
	fwrite( this->vector.get(), sizeof(dtype), (*this->shape)[0], file );
	
	fclose(file);
}

template<typename dtype>
void v<dtype, 1>::load(const char* raw_filename, bool load_shape){

	std::string filename = add_prefix("jkr_data/", raw_filename);

	filename += ".jkr";

	if(load_shape){

		//get shape file name
		std::string shape_filename = filename + ".shape";

		//get shape file
		FILE* shape_file = fopen(shape_filename.c_str(), "rb");

		//get shape from file
		unsigned int shape_copy;

		fread(&shape_copy, sizeof(unsigned int), 1, shape_file);

		(*this->shape)[0] = shape_copy;
		
		fclose(shape_file);
	}
	FILE* file = fopen(filename.c_str(), "rb");

	//read vector from file
	this->vector = std::make_unique<dtype[]>( (*this->shape)[0] );
	fread(this->vector.get(), sizeof(dtype), (*this->shape)[0], file);

	fclose(file);
}

template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator-(){

	return this->apply([](dtype element, int){
		return -element;
	});
}

/*


	scalar operations


*/

template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator+( dtype scalar ){

	int i;

	//creating new vector in which we'll store the results
	std::shared_ptr<Shape> result_shape( new Shape(1) );
	(*result_shape)[0] = (*this->shape)[0];
	v<dtype, 1> result( {}, result_shape );

	//iterate through the vector and add the scalar to each value
	//saving the result in a new vector
	for(i=0; i<(*this->shape)[0]; i++) result.vector[i] = this->vector[i] + scalar;

	return result;
}

template<typename dtype>
void v<dtype, 1>::operator+=( dtype scalar ){

	v<dtype, 1> result = *this + scalar;
	this->copy( result );
}

template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator-( dtype scalar ){

	int i;

	//creating new vector in which we'll store the results
	std::shared_ptr<Shape> result_shape( new Shape(1) );
	(*result_shape)[0] = (*this->shape)[0];
	v<dtype, 1> result( {}, result_shape );

	//iterate through the vector and add the scalar to each value
	//saving the result in a new vector
	for(i=0; i<(*this->shape)[0]; i++) result.vector[i] = this->vector[i] - scalar;

	return result;
}

template<typename dtype>
void v<dtype, 1>::operator-=( dtype scalar ){

	v<dtype, 1> result = *this - scalar;
	this->copy( result );
}

template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator*( dtype scalar ){

	int i;

	//creating new vector in which we'll store the results
	std::shared_ptr<Shape> result_shape( new Shape(1) );
	(*result_shape)[0] = (*this->shape)[0];
	v<dtype, 1> result( {}, result_shape );

	//iterate through the vector and add the scalar to each value
	//saving the result in a new vector
	for(i=0; i<(*this->shape)[0]; i++) result.vector[i] = this->vector[i] * scalar;

	return result;
}

template<typename dtype>
void v<dtype, 1>::operator*=( dtype scalar ){

	v<dtype, 1> result = *this * scalar;
	this->copy( result );
}

template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator/( dtype scalar ){

	int i;

	//creating new vector in which we'll store the results
	std::shared_ptr<Shape> result_shape( new Shape(1) );
	(*result_shape)[0] = (*this->shape)[0];
	v<dtype, 1> result( {}, result_shape );

	//iterate through the vector and add the scalar to each value
	//saving the result in a new vector
	for(i=0; i<(*this->shape)[0]; i++) result.vector[i] = this->vector[i] / scalar;

	return result;
}

template<typename dtype>
void v<dtype, 1>::operator/=( dtype scalar ){

	v<dtype, 1> result = *this / scalar;
	this->copy( result );
}

/*


	vector operations


*/

template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator+( v<dtype, 1>& other ){
	return this->apply( other, []( dtype a, dtype b) -> dtype{
		return a+b;
	} );
}

template<typename dtype>
void v<dtype, 1>::operator+=( v<dtype, 1>& other ){

	v<dtype, 1> result = *this + other;
	this->copy( result );
}

template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator-( v<dtype, 1>& other ){
	return this->apply( other, []( dtype a, dtype b) -> dtype{
		return a-b;
	} );
}

template<typename dtype>
void v<dtype, 1>::operator-=( v<dtype, 1>& other ){

	v<dtype, 1> result = *this - other;
	this->copy( result );
}


template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator*( v<dtype, 1>& other ){
	return this->apply( other, []( dtype a, dtype b) -> dtype{
		return a*b;
	} );
}

template<typename dtype>
void v<dtype, 1>::operator*=( v<dtype, 1>& other ){

	v<dtype, 1> result = *this * other;
	this->copy( result );
}


template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator/( v<dtype, 1>& other ){
	return this->apply( other, []( dtype a, dtype b) -> dtype{
		return a/b;
	} );
}

template<typename dtype>
void v<dtype, 1>::operator/=( v<dtype, 1>& other ){

	v<dtype, 1> result = *this / other;
	this->copy( result );
}

/*
	scalar comparative operators
*/
template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator<(dtype num){

	v<dtype, 1> result({ (*this->shape)[0] });
	unsigned int i;
	for(i=0; i<(*this->shape)[0]; i++) result[i] = (dtype)this->vector[i] < num;
	
	return result;
}
template<typename dtype>
v<dtype, 1> v<dtype, 1>::operator>(dtype num){

	v<dtype, 1> result({ (*this->shape)[0] });
	unsigned int i;
	for(i=0; i<(*this->shape)[0]; i++) result[i] = (dtype)this->vector[i] > num;
	
	return result;
}

#endif
