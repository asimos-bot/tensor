#ifndef V1_HPP
#define V1_HPP

namespace jkr{

	template<typename dtype>
	class v<dtype, 1>{

		private:
			std::unique_ptr<dtype[]> vector; //hold values
			std::shared_ptr<Shape> shape; //hold the number of dimensions, and how many elements in each
			std::mt19937 mt; //random number generator engine

		public:
			//constuctor
			v(std::initializer_list<unsigned int> list={}, std::shared_ptr<Shape> shape_arr = nullptr):shape(shape_arr){

				//initialize random number generator (for method rand)
				std::random_device rd;
				this->mt = std::mt19937( rd() );

				//if a list was given, determine size based on it
				if( list.size()!=0 ){

					this->shape.reset( new Shape(1) );
					(*this->shape)[0] = (int)list.begin()[0];

				}
				//if shape is set, allocate necessary memory for vector
				if( this->shape != nullptr )					
					//allocate memory for the vector
					this->vector = std::make_unique<dtype[]>( (*this->shape)[0] );
			}
			/*
				implementing range-based for
			*/
			//returns pointer to first value
			dtype* begin(){ return (*this->shape)[0] > 0 ? &this->vector[0] : nullptr; }
			//returns pointer imediatly after the last value
			dtype* end(){ return (*this->shape)[0] > 0 ? &this->vector[(*this->shape)[0]] : nullptr; }
			/*
				implementing range-based for
			*/
			//return number of owners of shape's space memory
			int num_owners();

			//return vector size
			unsigned int size();

			//return value at index
			dtype& operator[](unsigned int);

			//clone vector without taking ownership
			void copy( v<dtype, 1>&, bool=false);

			//return vector copy
			v<dtype, 1> copy();

			//return sum of all elements in vector
			dtype sum();

			//append value to the end of the vector
			void append(dtype);

			//fill the vector with random numbers between the given range
			void rand(dtype, dtype);
			
			//return highest value in matrix
			dtype amax();

			//return lowest value in matrix
			dtype amin();

			//turn this vector into a matrix
			v<dtype, 2> add_dimension();

			//return part of the matrix
			v<dtype, 1> cut(int, int);

			//apply given func to all elements of the vector and return result
			v<dtype, 1> apply( dtype (*)( dtype, int ) );

			//apply a given function element-wise on a current vector and a given vector
			v<dtype, 1> apply( v<dtype, 1>& , dtype(*)( dtype, dtype ) );
			
			//save vector in binary file
			void save(const char*, bool=true);

			//load vector from binary file
			void load(const char*, bool=true);
			
			//change sign
			v<dtype, 1> operator-();

			/*
				scalar operations
			*/

			//addition
			v<dtype, 1> operator+(dtype);
			void operator+=(dtype);

			//subtraction
			v<dtype, 1> operator-(dtype);
			void operator-=(dtype);

			//multiplication
			v<dtype, 1> operator*(dtype);
			void operator*=(dtype);

			//division
			v<dtype, 1> operator/(dtype);
			void operator/=(dtype);

			/*
				vector operations
			*/
			//addition
			v<dtype, 1> operator+( v<dtype, 1>& );
			void operator+=( v<dtype, 1>& );

			//subtraction
			v<dtype, 1> operator-( v<dtype, 1>& );
			void operator-=( v<dtype, 1>& );

			//multiplication
			v<dtype, 1> operator*( v<dtype, 1>& );
			void operator*=( v<dtype, 1>& );

			//division
			v<dtype, 1> operator/( v<dtype, 1>& );
			void operator/=( v<dtype, 1>& );
			
			/*
				scalar comparative operators
			*/
			v<dtype, 1> operator<(dtype);
			v<dtype, 1> operator>(dtype);
	};
}

#include "v1_methods.hpp"
#include "v1_helper.hpp"

#endif
